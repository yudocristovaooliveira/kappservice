﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace KappService
{
    public partial class KappService : ServiceBase
    {

        Logger _log = LogManager.GetCurrentClassLogger();


        public KappService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            ClientEmail c = new ClientEmail();
            c.InicializeEmailClient();
        }

        protected override void OnStop()
        {

        }
    }
}
