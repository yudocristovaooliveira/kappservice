﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapp.Lib.Models
{
    [PetaPoco.TableName("Contact")]
    [PetaPoco.PrimaryKey("idContact", AutoIncrement = true)]
    public class Contact : GenericTable
    {
       
        public int idContact { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string WebSite { get; set; }

    }
}
