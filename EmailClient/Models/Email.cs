﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapp.Lib.Models
{
    [PetaPoco.TableName("Email")]
    [PetaPoco.PrimaryKey("idEmail", AutoIncrement = true)]
    public class Email : GenericTable
    {

        public int idEmail { get; set; }
        public string toEmail { get; set; }
        public string fromEmail { get; set; }
        public string ccEmail { get; set; }
        public string bccEmail { get; set; }
        public string subjectEmail { get; set; }
        public string bodyEmail { get; set; }
        public DateTime receivedDate { get; set; }
        public string Attachments { get; set; }

    }
}
