﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapp.Lib.Models
{
    //
    [PetaPoco.TableName("email_has_contact")]
    [PetaPoco.PrimaryKey("Email_idEmail,Contact_idContact", AutoIncrement = false)]
    public class EmailHasContact
    {
        public int Email_idEmail { get; set; }
        public int Contact_idContact { get; set; }
    }
}
